package ru.ekfedorov.tm;

import ru.ekfedorov.tm.constant.TerminalConst;

public class Application {

    public static void main(final String[] args) {
        displayWelcome();
        run(args);
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void run(final String[] args) {
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        if (TerminalConst.CMD_HELP.equals(param)) displayHelp();
        if (TerminalConst.CMD_VERSION.equals(param)) displayVersion();
        if (TerminalConst.CMD_ABOUT.equals(param)) displayAbout();
    }

    private static void displayHelp() {
        System.out.println(TerminalConst.CMD_VERSION + " - Display program version.");
        System.out.println(TerminalConst.CMD_ABOUT + " - Display developer info.");
        System.out.println(TerminalConst.CMD_HELP + " - Display list of terminal commands.");
    }

    private static void displayVersion() {
        System.out.println("1.0.0");
    }

    private static void displayAbout() {
        System.out.println("Evgeniy Fedorov");
        System.out.println("ekfedorov@tsconsulting.com");
    }

}
